import numpy as np
from sklearn.metrics import confusion_matrix
import xgboost as xgb
from sklearn import metrics

def my_custom_gini(y, pred_dev):
    gini_score = 2*metrics.roc_auc_score(y, pred_dev, multi_class='ovr', average = 'weighted')-1
    #gini_score = 2*metrics.roc_auc_score(y, pred_dev, average = 'weighted')-1
    return gini_score

def metrics_validation(xgb_model, X_train, y_train, X_test, y_test, gini_train, gini_test, conf_matrix, Sen_test, UMS_test, PPV_test,\
    MulAcc_test, accuracy_train, accuracy_test):
    #Predictions
        y_train_pred = xgb_model.predict(X_train);
        y_test_pred  = xgb_model.predict(X_test);
         
        #Probabilities predictions
        y_train_pred_prob  = xgb_model.predict_proba(X_train);
        y_test_pred_prob   = xgb_model.predict_proba(X_test);
        
        #Gini parameter
        gini_score_train   = my_custom_gini(y_train, y_train_pred_prob);
        gini_score_test    = my_custom_gini(y_test, y_test_pred_prob);
        
        gini_train.append(gini_score_train)
        gini_test.append(gini_score_test)
        
        #Confusion Matrix, UMS and MulAcc parameters
        CM = confusion_matrix(y_test, y_test_pred);
        conf_matrix.append(CM)

        R = np.sum(CM, axis = 1);
        Se = [CM[0,0]/R[0], CM[1,1]/R[1], CM[2,2]/R[2], CM[3,3]/R[3], CM[4,4]/R[4]];
        Sen_test.append(Se)

        UMS = np.sum(Se)/len(Se);
        UMS_test.append(UMS)

        C = np.sum(CM, axis = 0);
        PPV = [CM[0,0]/C[0], CM[1,1]/C[1], CM[2,2]/C[2], CM[3,3]/C[3], CM[4,4]/C[4]];
        PPV_test.append(PPV)

        MulAcc = np.sum([CM[0,0],CM[1,1],CM[2,2],CM[3,3],CM[4,4]])/np.sum(C);
        MulAcc_test.append(MulAcc)
        
        Acc_train = metrics.accuracy_score(y_train,y_train_pred); 
        Acc_test  = metrics.accuracy_score(y_test,y_test_pred);

        accuracy_train.append(Acc_train)
        accuracy_test.append(Acc_test)
        
        GINI       = [gini_score_train, gini_score_test];
        UMS_MULACC = [UMS, MulAcc];
        
        #with open(path+'GINI_8_I_lead_V2.pickle', 'ab') as gini_sv:
        #    pickle.dump(GINI, gini_sv)
        #gini_sv.close();
        
        #with open(path+'UMS_MULACC_8_I_lead_V2.pickle', 'ab') as ums_mul_sv:
        #    pickle.dump(UMS_MULACC, ums_mul_sv)
        #ums_mul_sv.close();
        
        #with open(path+'Parameters_V2.pickle', 'ab') as parameters_sv:
        #    pickle.dump(results[n], parameters_sv)
        #parameters_sv.close();
        
        print(xgb_model);
        print("GINI_TRAIN: ",gini_score_train,";", " GINI_TEST: ",gini_score_test);
        print("UMS: ",UMS,";", " MulAcc: ",MulAcc);
        print("Accuracy_train: ", Acc_train,";", " Accuracy_test: ", Acc_test);

        return conf_matrix, gini_train, gini_test, Sen_test, UMS_test, PPV_test, MulAcc_test, accuracy_train, accuracy_test


def model_validate(results, n_iter, X_train, y_train, X_test, y_test):
    conf_matrix    = []
    gini_train     = []
    gini_test      = []
    Sen_test       = []
    UMS_test       = []
    PPV_test       = []
    MulAcc_test    = []
    accuracy_train = []
    accuracy_test  = []
    for n in np.arange(n_iter):
        xgb_model    = xgb.XGBClassifier(subsample = results[n]['subsample'], reg_lambda = results[n]['reg_lambda'], 
                                      reg_alpha = results[n]['reg_alpha'], n_estimators = results[n]['n_estimators'], 
                                      min_child_weight = results[n]['min_child_weight'], max_depth = results[n]['max_depth'], 
                                      learning_rate = results[n]['learning_rate'], gamma = results[n]['gamma'],
                                      eval_metric='mlogloss');
        
        xgb_model.fit(X_train, y_train);

        print("CASE ", n,":");
        print(results[n]);
        
        conf_matrix, gini_train, gini_test, Sen_test, UMS_test, PPV_test, MulAcc_test, accuracy_train, accuracy_test = \
            metrics_validation(xgb_model, X_train, y_train, X_test, y_test, gini_train, gini_test, conf_matrix, Sen_test, UMS_test, PPV_test, 
        MulAcc_test, accuracy_train, accuracy_test)

    return conf_matrix, gini_train, gini_test, Sen_test, UMS_test, PPV_test, MulAcc_test, accuracy_train, accuracy_test