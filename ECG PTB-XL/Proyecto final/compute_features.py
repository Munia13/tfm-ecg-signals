def process(i):
    
    try:
    
        print(i)
        x = [];
        features = np.ones(32);
        s_ecg = data_100_upd[i]
        #set_trace()
        s_ecg  = lfilter(b,a,s_ecg);
        s_in = s_ecg[0:n]  # Analisirako tartea hartu
        for sig in s_in:
            for z in sig:
                x.append(z)

        # Feature-en kalkulua

        x                        = np.array(x);
        idx = i;
        #idx = 'id_'+str(i);
        features[0] = idx;
        # COUNT
        c1, c2, c3b              = calculate_COUNT(x,fs);  
        c1 = np.round(c1,3); c2  = np.round(c2,3); c3b = np.round(c3b,3);
        features[1] = c1; features[2] = c2; features[3] = c3b;

        parameters               = calculate_CM_JEKOVA(x,fs,8);
        parameters               = np.round(parameters,3);
        features[4] = parameters[0]; features[5] = parameters[1]; features[6] = parameters[2]; 
        features[7] = parameters[3]; features[8] = parameters[4]; 

        N                        = calculate_EXP(x,fs);
        N                        = np.round(N,3);
        features[9] = N;

        N_expmod                 = calculate_EXPMOD(x,fs);
        N_expmod                 = np.round(N_expmod,3);
        features[10] = N_expmod;

        mav                      = calculate_MAV(x,fs);
        mav                      = np.round(mav,3);
        features[11] = mav;

        Npeak, stdAmpPeak, stdRR = calculate_peaks(x,fs);
        Npeak                    = np.round(Npeak,3);
        stdAmpPeak               = np.round(stdAmpPeak,3);
        stdRR                    = np.round(stdRR,3);
        features[12] = Npeak; features[13] = stdAmpPeak; features[14] = stdRR;

        psr,hilb                 =calculate_PSR_HILB(x,fs);
        psr                      = np.round(psr,3);
        hilb                     = np.round(hilb,3);
        features[15] = psr; features[16] = hilb;

        bCP, bWT                 = calculate_RESUS(x,fs);
        bCP                      = np.round(bCP,3);
        bWT                      = np.round(bWT,3);
        features[17] = bCP; features[18] = bWT;

        m = 2; r=0.2*statistics.stdev(x);

        #res                      = calculate_SampEn(x,m,r);
        #res                      = np.round(res,3);
        #features.append(res);

        spec                     = calculate_SPEC(x,fs);
        spec                     = np.round(spec,3);
        features[19] = spec[0]; features[20] = spec[1]; 
        features[21] = spec[2]; features[22] = spec[3];

        wL = 4;

        tci                      = calculate_TCI(x,fs,wL);
        tci                      = np.round(tci,3);
        features[23] = tci;

        tcsc                     = calculate_TCSC(x,fs);
        tcsc                     = np.round(tcsc,3);
        features[24] = tcsc;

        vfleak                   = calculate_VFLEAK(x);
        vfleak                   = np.round(vfleak,3);
        features[25] = vfleak;

        x1, x2                   = calculate_Xj(x,fs);
        x1                       = np.round(x1,3);
        x2                       = np.round(x2,3);
        features[26] = x1; features[27] = x2;

        x3, x4, x5               = calculate_Xi(x,fs);
        x3                       = np.round(x3,3);
        x4                       = np.round(x4,3);
        x5                       = np.round(x5,3);
        features[28] = x3; features[29] = x4; features[30] = x5;

        t_win = 4;

        Li                       = calculate_Li(x, fs, t_win);
        Li                       = np.round(Li,3);
        features[31] = Li;

        features = np.array(features).flatten();

        name = 'features_sv_1';

        with open('features_1.pickle', 'ab') as name:
            pickle.dump(features, name)
        name.close();
        
    except:   
        
        print('Error in signal number ',i);
        
        with open('sig_error_2.pickle', 'ab') as sig_error_sv_2:
            pickle.dump(i, sig_error_sv_2)
        sig_error_sv_1.close();

    return features