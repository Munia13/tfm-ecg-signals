﻿import numpy as np
from scipy.signal import lfilter, find_peaks, hilbert, butter
import math
import statistics
import pywt as pt

########################################################################################################################################################

def calculate_COUNT(signal,fs):

# Band-pass filtering and auxiliary counts, based on 
# "Real time detection of ventricular fibrillation and tachycardia"
# I. Jekova, V. Krasteva, Physiol. Meas. 25, 1167-1178, 2004.

# INPUT:
# - signal: filtered ecg
# - fs: sampling frequency
# - L: window length 
# - verbose: flag variable, for debugging purposes

# OUTPUT
# - c1, c2 and c3: calculated count1, count2 and count3 parameters

# by Felipe Alonso-Atienza (felipe.alonso@urjc.es)
# www.tsc.urjc.es/~felipe.alonso

# filter coefs definition, according to paper

# Adapted for Ali et al.


    #signal = signal.flatten();

    L = round(len(signal)/fs); 
    B = [0.5, 0, -0.5];
    A = [8, -14, 7];

# FSi signal

    Fsignal = lfilter(B,A,signal); 
    Fsignal_Abs = abs(Fsignal);

    Count1 = np.zeros(L)
    Count2 = np.zeros(L)
    Count3 = np.zeros(L)

    for count in list(range(L-1)):
        Intervalo_Fsignal_Abs = Fsignal_Abs[count*fs+1:count*fs+fs];
        max_Fsignal = max(Intervalo_Fsignal_Abs);
        mean_Fsignal = np.mean(Intervalo_Fsignal_Abs);
        md_Fsignal = np.mean(abs(Intervalo_Fsignal_Abs - mean_Fsignal));

        Count1[count+1] = sum(Intervalo_Fsignal_Abs >= 0.5*max_Fsignal); 
        Count2[count+1] = sum(Intervalo_Fsignal_Abs >= mean_Fsignal);
        Count3[count+1] = sum((Intervalo_Fsignal_Abs >= (mean_Fsignal - md_Fsignal)) * (Intervalo_Fsignal_Abs <= (mean_Fsignal + md_Fsignal)));

    Count1 = sum(Count1);
    Count2 = sum(Count2);
    Count3 = sum(Count3);

# MODIFIED FROM ORIGINAL: since c1, c2 and c3 depends on the window length
# they are normalized by L

    c1 = Count1/L;
    c2 = Count2/L;
    c3 = Count3/L;

    c3b = c1*c2/c3; # not interested in c3, but on c1*c2/c3 (see figure 4).
    
    return c1, c2, c3b

########################################################################################################################################################

def kolmogorov(s):
    
    n=len(s);
    c=1;
    l=1;

    i=0;
    k=1;
    k_max=1;
    stop=0;

    while stop==0:
        
        if s[i+k-1]!=s[l+k-1]:
            if k>k_max:
                k_max=k;

            i=i+1;
            
            if i==l:
                c=c+1;
                l=l+k_max;
                if l+1>n:
                    stop=1;
                else :
                    i=0;
                    k=1;
                    k_max=1;
                
            else :
                k=1;
            
        else :
            k=k+1;
            if l+k>n:
                c=c+1;
                stop=1;

    b=n/math.log(n,2);

    # a la Lempel and Ziv (IEEE trans inf theory it-22, 75 (1976), 
    # h(n)=c(n)/b(n) where c(n) is the kolmogorov complexity
    # and h(n) is a normalised measure of complexity.
    
    complexity=c/b;
    
    return c, complexity

########################################################################################################################################################                           

def calculate_CM_JEKOVA(signal,fs,wL):

# COMPLEXITY MEASURE parameters, based on: 

# 1) "Detecting ventricular tachycardia and fibrillation by complexity
#    parameter"
#    X.S. Zhang, Y.S. Zhu, N.V. Thakor and Z.Z. Wang
#    IEEE Trans. Biomed. Eng. 46(5): 548-55, 1999    

# 2) "Reliability of old and new ventricular fibrillation detection 
#    algorithms for automated external defibrillators"
#    A. Amann, R. Tratning, and K. Unterkofler,
#    Biomed Eng Online, 4(60), 2005.

# COVARIANCE, FREQUENCY, AREA, and KURTOSIS parameters, based on:

# 3) "Shock advisory tool: detection of life-threatening cardiac
#    arrhyrhmias and shock success prediction by means of a common
#    parameter set"
#    I. Jekova, Biomed. Sig. Proc. Control, 2:25-33, 2007
 
# 4) "Ventricular Fibrillation and Tachycardia Classification uning a
#    Machine Learning Approach", 
#    Q. Li, C. Rajagopalan and G.D. Clifford,
#    IEEE Trans. Biomed. Eng. (In Press)

# INPUT:
# - signal: ecg signal (preprocessed)
# - wL: window length, in seconds 

# OUTPUT
# - parameters: [1x5] vector containing: cm cvbin, frqbin, abin, 
#                                        and kurt paramters.

# by Felipe Alonso-Atienza (felipe.alonso@urjc.es) and
#    Eduardo Morgado (eduardo.morgado@urjc.es)
# www.tsc.urjc.es/~felipe.alonso

#--- Binary signal

    #signal = signal.flatten();

    n = len(signal);
    s = np.zeros(n)

    xi = signal - np.mean(signal);

    xmax = max(xi); 
    xmin = min(xi);

    Pc = sum(np.logical_and(0 < xi, xi < 0.1*xmax))
    Nc = sum(np.logical_and(0.1*xmin < xi, xi < 0))

    if (Pc+Nc)<0.4*n:
        th = 0;
    elif Pc<Nc:
        th = 0.2*xmax;
    else :
        th = 0.2*xmin;

    s[xi>=th] = 1;

    # Complexity measure
    #c, cm = kolmogorov(s);

    n=len(s);
    c=1;
    l=1;

    i=0;
    k=1;
    k_max=1;
    stop=0;

    while stop==0:
        
        if s[i+k-1]!=s[l+k-1]:
            if k>k_max:
                k_max=k;

            i=i+1;
            
            if i==l:
                c=c+1;
                l=l+k_max;
                if l+1>n:
                    stop=1;
                else :
                    i=0;
                    k=1;
                    k_max=1;
                
            else :
                k=1;
            
        else :
            k=k+1;
            if l+k>n:
                c=c+1;
                stop=1;

    b=n/math.log(n,2);

    # a la Lempel and Ziv (IEEE trans inf theory it-22, 75 (1976), 
    # h(n)=c(n)/b(n) where c(n) is the kolmogorov complexity
    # and h(n) is a normalised measure of complexity.
    
    complexity=c/b;

    # Convariance calculation
    cvbin = np.var(s);

    # Frequency calculation
    frqbin = sum(np.diff(s, n=1, axis=0) == 1);    
    frqbin = frqbin / wL;

    # Area calculation
    N = sum(s);
    abin = max(N,fs*wL-N);

    # Kurtosis calculation
    m_s = np.mean(signal);
    s_s = np.std(signal);
    kurt = np.mean((signal - m_s) ** 4)/pow(s_s,4) - 3;

    parameters = [complexity,cvbin,frqbin,abin,kurt];
    
    return parameters

######################################################################################################################################################## 

def calculate_EXP(signal,fs):

    # EXPONENTIAL parameter, based on:
    
    # 1) "Reliability of old and new ventricular fibrillation detection 
    #    algorithms for automated external defibrillators"
    #    A. Amann, R. Tratning, and K. Unterkofler,
    #    Biomed Eng Online, 4(60), 2005.
    
    # INPUT:
    # - signal: ecg signal (preprocessed)
    # - fs: sampling frequency
    # - wL: window length, in seconds 
    
    # OUTPUT
    # - exp parameter
    
    # by Felipe Alonso-Atienza (felipe.alonso@urjc.es)
    # www.tsc.urjc.es/~felipe.alonso

    signal = signal[:];

    # max value
    M = max(abs(signal)); 

    # tau = 3s
    tau = 3;
    L = len(signal);
    n = list(range(1,L+1));

    Es = M*np.exp(-abs(M-n)/(tau*fs));

    # Intersections
    above   = Es>=signal.flatten();
    peaks   = sum(np.diff(above, n=1, axis=0));

    N = peaks*60/(L/fs); #crossing per minutes

    return N

########################################################################################################################################################

def peakdet(v, delta):
    
    #PEAKDET Detect peaks in a vector
    #        [MAXTAB, MINTAB] = PEAKDET(V, DELTA) finds the local
    #        maxima and minima ("peaks") in the vector V.
    #        MAXTAB and MINTAB consists of two columns. Column 1
    #        contains indices in V, and column 2 the found values.
    #      
    #        With [MAXTAB, MINTAB] = PEAKDET(V, DELTA, X) the indices
    #        in MAXTAB and MINTAB are replaced with the corresponding
    #        X-values.
    #
    #        A point is considered a maximum peak if it has the maximal
    #        value, and was preceded (to the left) by a value lower by
    #        DELTA.

    # Eli Billauer, 3.4.05 (Explicitly not copyrighted).
    # This function is released to the public domain; Any use is allowed.

    maxtab = [];
    mintab = [];

    v = v.flatten(); # Just in case this wasn't a proper vector

    x = np.arange(0,len(v));
      
    if len([delta])>1:
      print('Input argument DELTA must be a scalar');
    if delta <= 0:
      print('Input argument DELTA must be positive');

    mn = np.inf; mx = -np.inf;
    mnpos = np.nan; mxpos = np.nan;

    lookformax = 1;

    for i in list(range(0,len(v))):
        this = v[i];
        if this > mx:
            mx = this; 
            mxpos = x[i]; 
        if this < mn: 
            mn = this; 
            mnpos = x[i]; 
        if lookformax:
            if this < mx-delta:
                if len(maxtab) == 0:
                    maxtab = [mxpos, mx];
                else :
                    maxtab = np.vstack([maxtab, [mxpos, mx]])
                mn = this; mnpos = x[i];
                lookformax = 0;
        else :
            if this > mn+delta:
                if len(mintab) == 0:
                    mintab = [mnpos, mn];
                else :
                    mintab = np.vstack([mintab, [mnpos, mn]])
                mx = this; mxpos = x[i];
                lookformax = 1;

    return maxtab

########################################################################################################################################################

def calculate_EXPMOD(signal,fs):

    # MODIFIED EXPONENTIAL parameter, based on:
    #
    # 1) "Reliability of old and new ventricular fibrillation detection 
    #    algorithms for automated external defibrillators"
    #    A. Amann, R. Tratning, and K. Unterkofler,
    #    Biomed Eng Online, 4(60), 2005.
    #
    # INPUT:
    # - signal: ecg signal (preprocessed)
    # - fs: sampling frequency
    # - wL: window length, in seconds 
    #
    # OUTPUT
    # - expmod parameter
    #
    # by Felipe Alonso-Atienza (felipe.alonso@urjc.es)
    # www.tsc.urjc.es/~felipe.alonso

    signal = signal[:];

    # normalization
    maximo = max(signal);
    signal = signal/maximo;

    #peak detection
    nm    = peakdet(signal,0.2); 
    mxpos = nm[:,0];
    mxpos = mxpos.astype(np.int64)

    if mxpos[0]==1:
        mxpos = mxpos[1:];

    # tau = 0.2s
    tau = 0.2;
    L = len(signal);

    if len(mxpos)==0:
        N = 1*60/(L/fs);
        return N

    # initialization, until the first maxima
    nm = mxpos;
    nm1 = nm[0];
    En = np.zeros((L,1));
    for i in np.arange(0,nm1):
        En[i] = signal[i]
    maxpos = nm1;


    # remaining samples
    n = np.arange(nm1-1,L);
    #offset = nm1-1;
    nmj = nm1;
    #i = 1;
    N = 1;
    fin = 0;

    #th = fs/5;

    while fin == 0:  
        
        Mj = signal[nmj];
        pos = 0
        for i in n:
            En[i] = Mj*np.exp(-(n[pos]-nmj)/(tau*fs));
            pos = pos + 1;
      
        ncj = [];
        val = [];
        ncj_1 = [];
        
        for i in np.arange(0,len(signal)):
            if signal[i] > En[i]:
                ncj.append(i);
                
        for i in np.arange(0,len(ncj)):
            if ncj[i]-nmj >= 10:
                val.append(i);
                
        for i in val:
            ncj_1.append(ncj[i]);       

        if np.size(ncj_1) == 0:
            fin = 1;
        else:
            if np.size(ncj_1)!=1:
                ncj_1 = ncj_1[0];

            aux = signal; 

            try:
                aux[0:ncj_1-1] = np.zeros(ncj_1-1);
            except:
                ncj_1 = ncj_1[0];
                aux[0:ncj_1-1] = np.zeros(ncj_1-1);

            nm = peakdet(aux,0.3);

            if len(nm) == 0:
                fin = 1;
                for i in np.arange(ncj_1,L):
                    En[i] = signal[i]
            else:
                nmj = []
                if len(nm) == 2:
                    if np.size(nm[0]) ==2:
                        pos = nm[0][0];
                    else:
                        pos = nm[0];

                    if pos >= ncj_1:
                        nmj.append(pos);
                else:   
                    pos = nm[:,0];
                    pos = pos.astype(np.int64)
               
                    for i in np.arange(0,len(pos)):
                        if pos[i]>=ncj_1:
                            nmj.append(pos[i]);

                if np.size(nmj) == 0:
                    fin = 1;
                    for i in np.arange(ncj_1,L):
                        En[i] = signal[i];
                else:
                    nmj = [];
                    if len(nm) == 2:
                        if np.size(nm[0]) ==2:
                            pos = nm[0][0];
                        else:
                            pos = nm[0];

                        if pos >= ncj_1:
                            nmj.append(pos);
                    else:   
                        pos = nm[:,0];
                        pos = pos.astype(np.int64)
               
                        for i in np.arange(0,len(pos)):
                            if pos[i]>=ncj_1:
                                nmj.append(pos[i]);

                    nmj = nmj[0];

                    nmj = nmj.astype(np.int64);
                    
                    for i in np.arange(ncj_1,nmj.astype(np.int64)):
                        En[i] = signal[i]
                        
                    n = np.arange(nmj,L);
                    #offset = nmj-1;
                    N = N +1;
                    #print(N)
                    #print("nmj: ", nmj)
                    maxpos = np.append(maxpos, nmj);
                    #print(maxpos)

    N = N*60/(L/fs);

    return N

########################################################################################################################################################

def calculate_MAV(signal,fs):

    # MEAN ABSOLUTE VALUE parameter, based on:
    #
    # 1) "Sequential algorithm for life threatening cardiac pathologies 
    #    detection based on mean signal strength and EMD functions"
    #    E. Anas, S. Lee, and M. Hasan,
    #    Biomed Eng Online, 9(1), 2010.
    #
    # INPUT:
    # - signal: ecg signal (preprocessed)
    # - fs: sampling frequency
    #
    # OUTPUT
    # - mav parameter
    #
    # by Felipe Alonso-Atienza (felipe.alonso@urjc.es)
    # www.tsc.urjc.es/~felipe.alonso
    #
    # Oct 2016: Adapted For Ali by U Irusta

    L = len(signal);
    T = round(L/fs);           
    N = T-1;

    Le = 2;
    Ne = (Le-1)*fs;

    mavi = np.zeros(N);

    for j in np.arange(N):
        xa = signal[j*Ne:(j+2)*Ne+1];
        xa = xa/max(abs(xa));
        mavi[j] = (1/Ne)*sum( abs(xa) );
    
    mav = np.mean(mavi);
    
    return mav

########################################################################################################################################################

def calculate_peaks(signal,fs):

    peaks, properties = find_peaks(signal, distance = round(0.15*fs), prominence = 0.140, width = round(0.04*fs) );
    pos = peaks

    pks = []
    for i in pos:
        pks.append(signal[i]);
        
    Npeak = len(pks);

    if Npeak>2:
        stdAmpPeak  = np.std(np.diff(pks, n=1, axis=0));
        stdRR       = np.std(np.diff(pos, n=1, axis=0));
    else:
        stdAmpPeak = -2;
        stdRR      = -2;

    return Npeak, stdAmpPeak, stdRR

########################################################################################################################################################

def calculate_PSR_HILB(signal,fs):

    # PHASE SPACE RECONSTRUCTION and HILBERT TRANSFORM parameter, based on:
    #
    # 1) "Detecting ventricular fibrillation by time-delay methods"
    #    A. Amann, R. Tratning, and K. Unterkofler,
    #    IEEE Trans Biomed Eng, 54(1): 174-77, 2007.
    #
    # 2) "A new ventricular fibrillation algorithm for automated external 
    #    defibrillators"
    #    A. Amann, R. Tratning, and K. Unterkofler,
    #    Computers in Cardiology, 559-562, 2005.
    #
    # INPUT:
    # - signal: ecg signal (preprocessed)
    # - fs: sampling frequency
    #
    # OUTPUT
    # - psr, hilb parameters
    #
    # by Felipe Alonso-Atienza (felipe.alonso@urjc.es)
    # www.tsc.urjc.es/~felipe.alonso


    #first downsampling ECG to 50 Hz;
    fd = 50;
    M = round(fs/fd);

    signal = signal[0:len(signal):M];
    fs = fd;

    #--- HILB
    X = hilbert(signal);

    xr = X.real; # Señal x(t)
    xi = X.imag; # Señal TH[x(t)]

    xr = xr - min(xr); # Restamos porque la primera casilla es (0,0)
    xi = xi - min(xi);

    deltaxr = max(xr)/39; # El "paso" para normalizar
    deltaxi = max(xi)/39;

    xr = np.floor(xr/deltaxr); # Sale entre 0 y 39 => OK
    xi = np.floor(xi/deltaxi); # Sale entre 0 y 39 => OK

    xi = np.array(xi).astype(np.int64)
    xr = np.array(xr).astype(np.int64)
    #--- 

    #--- PSR
    signal = xr;

    tau  = 0.5; # delay (seconds)
    ntau = tau*fs; # delay (samples)
    N    = len(signal);

    nt     = np.arange(0,N-ntau);
    ntplus = np.arange(ntau,N);

    nt     = nt.astype(np.int64)
    ntplus = ntplus.astype(np.int64)

    xt = [];
    xtp = [];

    for i in nt:
        xt.append(signal[i]);

    for i in ntplus:
        xtp.append(signal[i]);

    xt = np.array(xt).astype(np.int64)
    xtp = np.array(xtp).astype(np.int64)
    #---

    a = [];
    b = [];
    c = [];
    d = [];

    for i in np.arange(len(xt)):
        if xt[i] > 39:
            a.append(i);
            
    for i in np.arange(len(xtp)):
        if xtp[i] > 39:
            b.append(i);
            
    for i in np.arange(len(xi)):
        if xi[i] > 39:
            c.append(i);
            
    for i in np.arange(len(xr)):
        if xr[i] > 39:
            d.append(i);
            
    if len(a) != 0:
        for i in a:
            xt[i] = 39;
                       
    if len(b) != 0:
        for i in b:
            xtp[i] = 39;
                       
    if len(c) != 0:
        for i in c:
            xi[i] = 39;
                       
    if len(d) != 0:
        for i in d:
            xr[i] = 39;
                       


    A = np.zeros((40,40), dtype=int)
    for i in np.arange(len(xtp)):
        A[xt[i],xtp[i]]=1;

    B = np.zeros((40,40), dtype=int)
    for i in np.arange(len(xi)):
        B[xr[i],xi[i]]=1;   

    psr  = sum(sum(A))/1600;
    hilb = sum(sum(B))/1600;

    return psr,hilb

########################################################################################################################################################

def calculate_RESUS(signal,fs):

    # Slope and Frequency domain Features, from
    # "A high-temporal resolution algorithm to discriminate shockable
    # from nonshockable rhythms in adults and children", Irusta U. et al.
    #
    # INPUT:
    # - signal: filtered ecg [mV]
    # - fs: sampling frequency [sample/s]
    #
    # OUTPUT:
    # - bCP, bWT, bW: features 
    # Added pAS for asystole and does not return bW (not used)
    #
    # by Unai Irusta and Unai Ayala (unai.irusta@ehu.es)
    #
    # Oct 2016: Adapted For Ali by U Irusta

    valC  = 0.0055; ### Para bCP
    valPT = 47;  ### Para bWT

    signal = signal.flatten();


    L     = len(signal);
    L_w   = 2*fs;  # Use 2-s windows for bCP y bWT

    ### Slope domain feature
    slope = np.diff(signal, n=1, axis=0);
    slope = slope ** 2; 
    slope = np.append(slope, slope[-1]);

    n_w = np.floor(len(slope)/L_w);

    bCP = [];
    for i in np.arange(n_w, dtype=int):
        
        cont = 0;
        slp_w = slope[i*L_w:(i+1)*L_w];
        
        for i in slp_w:
            if i < valC*max(slp_w):
                cont = cont+1;
                
        bCP.append(cont/L_w);

    bCP = min(bCP);

    ### Time domain feature (baseline)
    tr_sec = 2;
    x_1 = np.ones(tr_sec*fs, dtype=int)*signal[0];
    x_2 = np.append(x_1, signal.flatten());
    x = np.append(x_2, np.ones(tr_sec*fs, dtype=int)*signal[-1])


    b, a = butter(5, [13/fs, 60/fs], 'bandpass', True);
    signal_f = lfilter(b,a,signal);
    signal_f = signal_f[tr_sec*fs:-1-tr_sec*fs];

    n_w = np.floor(len(signal_f)/L_w);
    bWT = [];
    for i in np.arange(n_w, dtype=int):
        sig_w  = signal_f[(i)*L_w:(i+1)*L_w];
        if len(bWT)==0:
            bWT = [(np.percentile(sig_w,50+valPT/2, interpolation='midpoint')- 
                   np.percentile(sig_w,50-valPT/2, interpolation='midpoint'))/max(abs(sig_w))];
        else:                                                                          
            bWT.append((np.percentile(sig_w,50+valPT/2, interpolation='midpoint')-
                       np.percentile(sig_w,50-valPT/2, interpolation='midpoint'))/max(abs(sig_w)));

    bWT = max(bWT);

    return bCP, bWT


########################################################################################################################################################

def calculate_SampEn(X,m,r):
    #""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    # PROPÓSITO:
    # Función que calcula la estimación de la entropía muestral SampEn de una 
    # señal. 
    # FORMA DE USO:
    #	[res] = SampEn(X)
    #
    # ARGUMENTOS...
    # ...DE ENTRADA: 
    #       .-X ---> señal de la que se pretende estimar la SampEn.
    # ...DE  SALIDA: 
    #       .-sampen  ---> valor de la SampEn calculada.
    #
    # COMENTARIOS:
    #Lake, D. E., J. S. Richman, et al. (2002).
    #"Sample entropy anallysis of neonatal heart rate variability." 
    #Am. J. Physiol. Heart. Circ. Physiol. 283: 789-797.
    #
    # Los parámetros han sido fijados siguiendo las referencias: Pincus 91,
    # Pincus 94, Pincus 01.
    # 
    # m=2  y r=0.2*SD, siendo SD la desviación estandar de los datos.
    #
    #""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    # AUTHORS:
    # REBECA GOYA ESTEBAN
    # OSCAR BARQUERO PEREZ
    #
    # FECHA: 27-09-2007
    #
    # VERSION 2.0
    #
    #""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    #Parametros
    sd = statistics.stdev(X);
    r  = r*sd;
    N=len(X);

    #convierte en vector columna
    X = np.column_stack(X);


    B_m_i = np.zeros(N-m);
    A_m_i = np.zeros(N-m);

    #se crea una matriz que contendra todos los vectores a ser comparados
    #entre si.
    for n in np.arange(2):
        
        M   = np.zeros((N-m,m+n));
        tam = M.shape;
        f = tam[0];
        
        
        for i in np.arange(f):
             M[i,:] = X[:,i:i+m+n];
        #calculo de la medida de correlacion.

        for i in np.arange(f):
            #se construye una matriz cuyas filas contien el vector a ser comparado con el resto de los
            #vectores, replica la matriz dada con dimensiones fx1.
            Mi   = np.kron(np.ones((f,1)),M[i,:]);
            #para cada fila de la matrix el maximo entre las columnas de la matriz de
            #diferencias
            dist = np.amax(abs(Mi-M),1);
            #para eliminar las autocomparaciones
            dist[i] = None;
            if n == 0:           
                B_m_i[i] = sum(dist<=r)/(N-m-1);
            else:
                A_m_i[i] = sum(dist<=r)/(N-m-1);

    B_m = np.mean(B_m_i);
    A_m = np.mean(A_m_i);

    res = np.log(B_m) - np.log(A_m);

    return res

########################################################################################################################################################

def calculate_SPEC(senal,fs):

    # SPECTRAL ALGORITHM, based on:
    #
    # 1) "Algorithmic sequential decision making in the frequency domain for
    #    life threatening ventricular arrythmias and imitative artifacts: a
    #    diagnostic system",
    #    S. Barro, R. Ruiz, D. Cabello, and J. Mira, 
    #    Journal of Biomed Eng., 11(4): 320-8, 1989
    #
    # 2) "Reliability of old and new ventricular fibrillation detection 
    #    algorithms for automated external defibrillators"
    #    A. Amann, R. Tratning, and K. Unterkofler,
    #    Biomed Eng Online, 4(60), 2005.
    #
    # INPUT:
    # - senal: ecg signal (preprocessed)
    # - fs: sampling frequency
    #
    # OUTPUT
    # - M, A1, A2, A3 parameters
    #
    # by Felipe Alonso-Atienza (felipe.alonso@urjc.es)
    # www.tsc.urjc.es/~felipe.alonso

    senal = np.column_stack(senal);

    L       = len(senal);
    ventana = np.hamming(L); 
    ventana = np.column_stack(ventana);

    # Multiply by hamming window and calculate FFT

    NFFT = int(2 ** 13); 
    Y    = np.fft.fft(senal*ventana,NFFT);  #senal enventanada
    Y    = Y[:, 0:int(NFFT/2)];
    f    = np.linspace(0,fs/2,int(NFFT/2));

    # Definition of amplitude spectrum according to the original paper

    Amp = abs(Y.real) + abs(Y.imag);


    # Insignificant components reduction: those below 5# of the max. ampl.

    cond   = (f>=0.5) & (f<=9)
    aux    = np.array(range(len(cond)));
    iomega = aux[cond];
    romega = f[np.where(cond)];

    Amax = np.amax(Amp[:,iomega]);
    pos  = np.argmax(Amp[:,iomega]);
    th          = 0.05*Amax;
    Amp = (Amp<th).choose(Amp,0);

    # Parameters

    omega   = romega[pos];
    jmax    = min(20*omega,100);

    # M: 0-min(20*omega,100)

    cond   = (f<=jmax)
    aux    = np.array(range(len(cond)));
    js = aux[cond];
    ws = f[np.where(cond)];
    aj = Amp[:,js].flatten();

    M = (1/omega)*np.dot(aj,ws) /sum(aj);

    # A1

    cond   = (f>=0.5) & (f<=omega/2);
    aux    = np.array(range(len(cond)));
    jsnum  = aux[cond];
    ws     = f[np.where(cond)];
    ajnum  = Amp[:,jsnum].flatten();

    cond   = (f>=0.5) & (f<=jmax);
    aux    = np.array(range(len(cond)));
    jsden  = aux[cond];
    ajden  = Amp[:,jsden].flatten();

    A1 = (1/omega)*np.dot(ajnum,ws) /sum(ajden);

    # A2

    cond   = (f>=0.7*omega) & (f<=1.4*omega);
    aux    = np.array(range(len(cond)));
    jsnum  = aux[cond];
    ws     = f[np.where(cond)];
    ajnum  = Amp[:,jsnum].flatten();

    A2 = (1/omega)*np.dot(ajnum,ws) /sum(ajden);

    # A3

    cond   = (f>=2*omega) & (f<=8*omega);
    aux    = np.array(range(len(cond)));
    jsnum  = aux[cond];
    ws     = f[np.where(cond)];
    ajnum  = Amp[:,jsnum].flatten();

    A3 = (1/omega)*np.dot(ajnum,ws) /sum(ajden);

    # all
    spec = [M, A1, A2, A3];

    return spec

########################################################################################################################################################

def calculate_TCI(xf,fs,wL):

    # THRESHOLD CROSSING INTERVAL parameter, based on:
    #
    # 1) "Ventricular tachycardia and fibrillation detection by a sequential 
    #    hypothesis testing algorithm"
    #    N.V Thakor, Y.S. Zhu, and K.Y. Pan, 
    #    IEEE Trans Biomed Eng, 37(9): 837-843, 1990.
    #
    # 2) "Reliability of old and new ventricular fibrillation detection 
    #    algorithms for automated external defibrillators"
    #    A. Amann, R. Tratning, and K. Unterkofler,
    #    Biomed Eng Online, 4(60), 2005.
    #
    # INPUT:
    # - xf: ecg signal (preprocessed)
    # - fs: sampling frequency
    # - wL: window length, in seconds 
    # - verbose: debugging variable (1: plot; 0: default, not ploting)
    #
    # OUTPUT
    # - tci parameter
    #
    # by Felipe Alonso-Atienza (felipe.alonso@urjc.es)
    # www.tsc.urjc.es/~felipe.alonso

    wl = 1*fs;  # 1-sec window samples
    wa = 3*fs;  # 3-sec window samples


    L = wL-3+1; # number of 3-sec windows in wl segment  

    becg1 = np.zeros((wl,), dtype=int);
    becg2 = np.zeros((wl,), dtype=int);
    becg3 = np.zeros((wl,), dtype=int);

    tci6 = np.zeros((L,));

    for j in np.arange(L):
        
        wsamples1 = np.arange(j*wl,(j+1)*wl);
        wsamples2 = np.arange((j+1)*wl,(j+2)*wl);
        wsamples3 = np.arange((j+2)*wl,(j+3)*wl);
        
        stage1 = xf[wsamples1] - np.mean(xf[wsamples1]); 
        maxv = max(stage1); 
        th1 = 0.2*maxv;
        becg1 = (stage1>th1).choose(becg1,1);
        
        stage2 = xf[wsamples2] - np.mean(xf[wsamples2]); 
        maxv = max(stage2); 
        th2 = 0.2*maxv;
        becg2 = (stage2>th2).choose(becg2,1);
        
        stage3 = xf[wsamples3] - np.mean(xf[wsamples3]); 
        maxv = max(stage3); 
        th3 = 0.2*maxv;
        becg3 = (stage3>th3).choose(becg3,1);
        
        becg = np.append(becg1,becg2);
        becg = np.append(becg,becg3);
        
        aux = np.append(0,np.diff(becg, n=1, axis=0));
        
        cond   = (aux[0:wl]==-1)
        aux_0  = np.array(range(len(cond)));
        s1     = aux_0[cond];
        
        if len(s1) == 0:
            t1 = 1;
        else : 
            t1 = (wl-s1[-1])/fs;
        
        cond   = (aux[wl:2*wl] != 0);
        aux_0  = np.array(range(len(cond)));
        index  = aux_0[cond];
        s2     = aux[wl:2*wl];
        pulses = s2[np.where(cond)];
        
        if pulses[0] == -1 and pulses[-1] == -1:
            t2 = 0;
            t3 = (wl-index[-1])/fs;
            N = (len(pulses)+1)/2;
        elif pulses[0] == 1 and pulses[-1] == 1:
            t2 = index[0]/fs;
            t3 = 0;
            N = (len(pulses)+1)/2;
        elif pulses[0] == -1 and pulses[-1] == 1:
            t2 = 0;
            t3 = 0;
            N = (len(pulses)+2)/2;
        elif pulses[0] == 1 and pulses[-1] == -1:
            t2 = index[0]/fs;
            t3 = (wl-index[-1])/fs;
            N = (len(pulses)+2)/2;
        else:
            disp('This should not be happening!')
        
        cond   = (aux[2*wl:3*wl]==1);
        aux_0  = np.array(range(len(cond)));
        s4     = aux_0[cond];
        
        if len(s4) == 0:
            t4 = 1;
        else:
            t4 = s4[0]/fs;
            
        tci6[j] = 1000/((N-1)+(t2/(t1+t2))+(t3/(t3+t4)));
        
        becg1 = np.zeros((int(wa/3),), dtype=int);
        becg2 = np.zeros((int(wa/3),), dtype=int);
        becg3 = np.zeros((int(wa/3),), dtype=int);

    tci = np.mean(tci6);

    return tci

########################################################################################################################################################

def calculate_TCSC(xf,fs):

    # THRESHOLD CROSSING SAMPLE COUNT parameter, based on:
    #
    # 1) "A simple time domain algorithm for the detection of ventricular
    #    fibrillation in electrocardiogram"
    #    Arafat et al, 
    #    Signal, Image and Video Processing, 5: 1-10, 2011.
    #
    #
    # INPUT:
    # - xf: ecg signal (preprocessed)
    # - fs: sampling frequency
    # - wL: window length, in seconds. 
    # - verbose: debugging variable (1: plot; 0: default, not ploting)
    #
    # OUTPUT
    # - tcsc parameter

    xf = np.column_stack(xf);
    Ls = 3;
    Le = round(np.size(xf)/fs);

    wls = Ls*fs;
    w   = np.zeros((Ls*fs,), dtype=int); 

    tt = np.linspace(0,Le,Le*fs);
    t  = np.linspace(0,Ls,Ls*fs);
    ia = ((t<0.25)).nonzero();
    #ia = ia[0].item();
    ib = ((t>=0.25) & (t<=(Ls-0.25)));
    ic = ((t>Ls-0.25)).nonzero();
    #ic = ic[0].item();

    # 1/4 segundos
    for j in ia:
        w[j] = 0.5*(1-np.cos(4*math.pi*t[j]));
    
    w[ib] = 1;

    for j in ic:
        w[j] = 0.5*(1-np.cos(4*math.pi*t[j]));

    V0 = 0.2;
    N = np.zeros((Le-2,), dtype=int); 

    for j in np.arange(Le-3+1):
        wsamples = range(j*1*fs,(j+3)*1*fs);
        xf = xf.flatten();
        wecg = (np.column_stack(xf[wsamples])*w).flatten();
        xf = np.column_stack(xf);

        maxv = max(abs(wecg));
        wecg = wecg/maxv;

        becg = np.zeros((np.size(wecg),), dtype=int);
        becg[abs(wecg)>V0] = 1;

        N[j] = sum(becg)*100/wls;

    tcsc = np.mean(N);

    return tcsc

########################################################################################################################################################

def calculate_VFLEAK(xf):

    # VF LEAK parameter, based on:
    #
    # 1) "Computer detection of ventricular fibrillation"
    #    S. Kuo, D. Dillman, Computers in Cardiology, 1978, 2747-2750.
    #
    # 2) "Reliability of old and new ventricular fibrillation detection 
    #    algorithms for automated external defibrillators"
    #    A. Amann, R. Tratning, and K. Unterkofler,
    #    Biomed Eng Online, 4(60), 2005.
    #
    # INPUT:
    # - signal: ecg signal (preprocessed)
    #
    # OUTPUT
    # - vfleak parameter

    num = sum(abs(xf[1:]))
    den = sum(abs(xf[1:] - xf[0:-1]))

    N = np.floor((math.pi*(num)/(den))+1/2);
    N = int(N);

    num = sum(abs(xf[N:] + xf[0:-N]));
    den = sum(abs(xf[N:]) + abs(xf[0:-N]));

    vf = num/den;

    return vf

########################################################################################################################################################

def calculate_Xi(xf,fs):

    # Slope and Frequency domain Features, from
    # "A Reliable Method for Rhythm Analysis During Cardiopulmonary 
    # Resuscitation", Ayala U. et al.
    #
    # INPUT:
    # - signal: filtered ecg [mV]
    # - fs: sampling frequency [sample/s]
    # - verbose: flag variable, for debugging purposes
    #
    # OUTPUT:
    # - x3, x4, x5: frequency domain features 


    xf = np.column_stack(xf);

    # frequency domain features
    Nfft = 2048;
    Sfft = np.fft.fftshift(np.fft.fft(xf*(np.hamming(np.size(xf))),Nfft));

    ff   = np.linspace(-fs/2,fs/2,Nfft);
    Pss  = abs(Sfft)**2;

    df   = ff[1]-ff[0];
    aux  = Pss*df;
    aux  = aux.flatten();
    area = sum(aux)/2;   # f>0
    Pss  = Pss/area;     # normalization to unit Pss

    ind = ((ff>=0) & (ff<=30)).nonzero();
    f = ff[ind];         # just consider 0<f<30 Hz
    Pss = Pss.flatten();
    Pss = Pss[ind];

    # frequency ranges
    f_1_10  = ((f>=1) & (f<=10)).nonzero();
    f_1_10  = f_1_10[0];
    f_2_7   = ((f>=2.5) & (f<=7.5));
    f_12_30 = ((f>=12) & (f<=30));

    val = max(Pss[f_1_10]);
    pos = np.argmax(Pss[f_1_10]);

    x3 = f[f_1_10[pos]];
    x4 = sum(Pss[f_2_7]*df);
    x5 = sum(Pss[f_12_30]*df);

    return x3,x4,x5

########################################################################################################################################################

def calculate_Xj(xf,fs):

    # Slope and Frequency domain Features, from
    # "A Reliable Method for Rhythm Analysis During Cardiopulmonary 
    # Resuscitation", Ayala U. et al.
    #
    # INPUT:
    # - signal: filtered ecg [mV]
    # - fs: sampling frequency [sample/s]
    #
    # OUTPUT:
    # - x1, x2: slope domain features 

    xf = np.column_stack(xf);

    # Promediador y cálculo de la pendiente

    aMs = 100;    # Promediamos 100ms
    prC = 10;
    bP  = np.ones(round(aMs*fs/1000), dtype=int);

    # Calculo pendiente al cuadrado

    slope = np.diff(xf);
    slope = slope.flatten();
    slope = np.append(slope, slope[-1]);
    slope = slope**2;

    # Promediado de la pendiente (eliminando transitorio)

    tr_sec  = 2; # 2 segundos de transitorio
    slope   = np.concatenate((np.ones(tr_sec*fs, dtype=int)*slope[0], slope, np.ones(tr_sec*fs, dtype=int)*slope[-1]));
    slope   = lfilter(bP,1,slope);
    slope   = slope[tr_sec*fs:-1-(tr_sec*fs-1)];

    # ensbCP es el percentil 10

    x1 = np.percentile(slope,prC,interpolation='midpoint')/max(slope);

    # nPA Modificado

    minAmp            = 0.1*max(slope);  # 10# del normalizado
    minDist           = fs/10; 
    peaks, properties = find_peaks(slope, distance = minDist, height = minAmp);
    x2                = np.size(peaks);

    return x1,x2

########################################################################################################################################################

def calculate_Li(xf, fs, t_win):

    # Feature from
    # "An Algorithm Used for Ventricular Fibrillation
    # Detection Without Interrupting Chest Compression", Li et al.
    #
    # INPUT:
    # - signal: filtered ecg [mV]
    # - fs: sampling frequency [sample/s]
    # - verbose: flag variable, for debugging purposes
    #
    # OUTPUT:
    # - Li: mean residues (modified from original) 
    #
    # by Unai Ayala and Unai Irusta (unai.irusta@ehu.es)
    #
    # Oct 2016: Adapted For Ali by U Irusta


    # Parameters
    nMax=6; wave='db5';

    # Transformada wavelet
    # To select the level that most fits our signal based on the wave
    
    max_level = pt.swt_max_level(len(xf)); #  In this case the result is 7

    swt_ecg_v1 = pt.swt(xf, wave);

    # We convert the list of arrays into a unique array, fetching only the detailed coefficients values

    swt_ecg = [];
    for i in np.arange(0,max_level):
        swt_ecg.append(swt_ecg_v1[i][1]);

    swt_ecg = np.concatenate(swt_ecg[:], axis=0);
    swt_ecg = np.reshape(swt_ecg, (max_level,len(xf))); # Le damos forma de matriz

    tfm_ecg = abs(swt_ecg)**2;

    # Calculamos picos y seleccionamos nMax mayores
    # Debe poder seleccionarse intervalo (+,-)t_win*fs alrededor

    # Primero damos la vuelta a la matriz, convirtiendo columnas en filas y vs, y lo convertimos en un único arra --  para que seleccione correctamente los picos.

    tfm_ecg_T = tfm_ecg.T;
    tfm_ecg_T = tfm_ecg_T.flatten();

    peaks, properties = find_peaks(tfm_ecg_T[t_win*fs:-1-t_win*fs+1], distance = round(0.15*fs), height = 0);

    idx = properties['peak_heights'].argsort()[::-1]; # Ordenamos de manera descendente las amplitudes de los picos y devolvemos los índices de los mismos.

    if len(peaks) < nMax: 
        nMax = len(peaks);

    peaks = peaks[idx[0:nMax]]+t_win*fs;

    # Calculamos el template

    sw_j = [];
    swt_ecg_T = swt_ecg.T;
    swt_ecg_T = swt_ecg_T.flatten();

    for i in np.arange(0,nMax):
        interval  = np.arange(-t_win*fs,t_win*fs)+peaks[i];
        sw_j.append(swt_ecg_T[interval]);

    sw_j = np.reshape(sw_j, (nMax,len(np.arange(-t_win*fs,t_win*fs))));
    tw_j = np.mean(sw_j.T,1);

    # Calculamos las correlaciones y residuos
    xx_tw = np.correlate(tw_j,tw_j, mode = 'full'); 
    xx_tw = xx_tw/max(xx_tw);

    xy_tw = [];
    res   = [];

    for i in np.arange(0,nMax):
        aux = np.correlate(tw_j,sw_j[i], mode = 'full');
        xy_tw = np.append(xy_tw, aux);
        xy_tw = np.reshape(xy_tw, (i+1,len(np.correlate(tw_j,sw_j[i], mode = 'full'))));
        xy_tw[i] = xy_tw[i]/max(xy_tw[i]);
        res.append(sum(abs(xx_tw-xy_tw[i])));

    # Modified from original
    Li = np.percentile(res,50, interpolation='midpoint');

    return Li




















