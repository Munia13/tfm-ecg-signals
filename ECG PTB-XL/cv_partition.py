import pandas as pd
import numpy as np
from IPython.core.debugger import set_trace

def RandomGroupKFold_split(groups, n, y):  
    """
    Random analogous of sklearn.model_selection.GroupKFold.split.

    :return: list of (train, test) indices
    """
    groups = pd.Series(groups)
    ix = np.arange(len(groups))
    unique = np.unique(groups)
    np.random.RandomState(None).shuffle(unique)

    unique_total, counts_total = np.unique(y, return_counts=True);

    norm_pro = round((counts_total[0]/len(y))*0.85, 3); # print("norm_pro: ", norm_pro);
    mi_pro   = round((counts_total[1]/len(y))*0.85, 3); # print("mi_pro: ", mi_pro);
    sttc_pro = round((counts_total[2]/len(y))*0.85, 3); # print("sttc_pro: ", sttc_pro);
    cd_pro   = round((counts_total[3]/len(y))*0.85, 3); # print("cd_pro: ", cd_pro);
    hyp_pro  = round((counts_total[4]/len(y))*0.85, 3); # print("hyp_pro: ", hyp_pro);

    # result = []
    # results = np.ones((10,2), dtype=int);
    res_train = [];
    res_test  = [];

    tr = 0;
 
    for split in np.array_split(unique, n):
        mask        = groups.isin(split)
        train, test = ix[~mask], ix[mask];
        
        # Calculamos proporciones por anotaci�n para verificar si cumple con las proporciones definidas; en este caso, que la proporci�n por tipo de ritmo sea mayor al
        # 85%
        unique_v1, counts_v1 = np.unique(y[train], return_counts=True);

        norm_pro_t = int(len(train)*norm_pro); 
        mi_pro_t   = int(len(train)*mi_pro); 
        sttc_pro_t = int(len(train)*sttc_pro); 
        cd_pro_t   = int(len(train)*cd_pro); 
        hyp_pro_t  = int(len(train)*hyp_pro); 
        
        if not counts_v1[0] >= norm_pro_t and counts_v1[1] >= mi_pro_t and counts_v1[2] >= sttc_pro_t and counts_v1[3] >= cd_pro_t and counts_v1[4] >= hyp_pro_t:

            print("NOK");
            break;
        else:    
            tr = tr+1;

        res_train.append(train);
        res_test.append(test);
        # result.append((train, test))
        
    return tr, res_train, res_test